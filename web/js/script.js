jQuery(document).ready(function() {
	jQuery('.load-cnt__counter').addClass('text-success');
	setTimeout(function(){
		jQuery('.load-cnt__counter').removeClass('text-success');
	}, 500);
});


jQuery('.buttoncounter').click(function(){
	var elm = jQuery(this);
	var url = elm.data('url')+'/site/addbuttoncounter/';
	jQuery.ajax({
		type: 'POST',
		url: url,
		data: {},
		success: function(data){
			jQuery('.input-cnt__counter').text(data);
			jQuery('.input-cnt__counter').addClass('text-success');
			setTimeout(function(){
				jQuery('.input-cnt__counter').removeClass('text-success');
			}, 500);
		}, 
		error: function(){
			alert('error');
		}
	});
});