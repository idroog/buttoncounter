<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "counters".
 *
 * @property int $id
 * @property int $input_cnt
 * @property int $load_cnt
 */
class Counters extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'counters';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['input_cnt', 'load_cnt'], 'required'],
            [['input_cnt', 'load_cnt'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'input_cnt' => 'Input Cnt',
            'load_cnt' => 'Load Cnt',
        ];
    }
}
